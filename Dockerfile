FROM frolvlad/alpine-python3
ADD . /code
WORKDIR /code
ENV no_proxy=redis
RUN pip install -r requirements.txt 
EXPOSE 5000
CMD ["python3", "run.py"]


